<?php
declare(strict_types=1);

namespace RoflCopter24\ContaoCssPurgerBundle\Event\Subscriber;

use RoflCopter24\ContaoCssPurgerBundle\Service\StylePurgeService;
use Illuminate\Support\Str;
use Masterminds\HTML5;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class KernelResponseSubscriber implements EventSubscriberInterface
{
    /**
     * @var StylePurgeService
     */
    private StylePurgeService $cssPurger;

    private string $execDir;

    private bool $enabled;

    public function __construct(StylePurgeService $cssPurger, ParameterBagInterface $parameterBag)
    {
        $this->cssPurger = $cssPurger;
        $this->execDir = $parameterBag->has('contao.web_dir') ? $parameterBag->get('contao.web_dir') : $parameterBag->get('kernel.root_dir');
        $this->enabled = $parameterBag->has('contao_css_purger.enabled') ? $parameterBag->get('contao_css_purger.enabled'): false;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::RESPONSE => [
                ['onKernelResponsePost', -10]
            ]
        ];
    }

    public function onKernelResponsePost(ResponseEvent $event)
    {
        // we only care about the assembled master request if the feature is enabled at all
        if (!$this->enabled || !$event->isMasterRequest()) {
            return;
        }

        $response = $event->getResponse();

        // handle only html
        $contentType = $response->headers->get('Content-Type');
        if (!$contentType || !Str::contains($contentType, 'text/html')
            || Str::contains($event->getRequest()->getPathInfo(), '_profiler')
            || Str::contains($event->getRequest()->getPathInfo(), 'contao')) {
            return;
        }

        $html = $response->getContent();
        if (!$html) {
            return;
        }

        $domCrawler = new Crawler($html);
        try {
            $styleSheets = $domCrawler->filter('link[rel*="stylesheet"]')->extract(['href']);
        } catch (\InvalidArgumentException $exception) {
            $styleSheets = [];
        }
        try {
            $styleTags = $domCrawler->filter('style')->extract(['_text']);
        } catch (\InvalidArgumentException $exception) {
            $styleTags = [];
        }

        if ((!$styleSheets || count($styleSheets) == 0) && (!$styleTags || count($styleTags) == 0)) {
            return;
        }

        $fs = new Filesystem();
        $styleSheets = array_filter($styleSheets, function (string $fileName) {
            // handle only internal stylesheets as php external file access may not be allowed
            if (Str::startsWith($fileName, 'http')) {
                return false;
            }
            return true;
        });

        $style = collect($styleSheets)
            ->map(function (string $cssFile) use ($html, $fs): string {
                $cssFile = Str::before($cssFile, '?');
                if (!$fs->isAbsolutePath($cssFile)) {
                    $cssFile = "$this->execDir/$cssFile";
                } else {
                    $cssFile = "$this->execDir$cssFile";
                }
                if (!$fs->exists($cssFile)) {
                    throw new FileNotFoundException("CSS File $cssFile was not found!");
                }
                $css = file_get_contents($cssFile);
                return $this->cssPurger->removeUnusedCSSFromHtml($html, $css);
            })
            ->join('');

        $style = $style.collect($styleTags)
            ->map(function(string $css) use ($html): string {
                return $this->cssPurger->removeUnusedCSSFromHtml($html, $css);
            })
            ->join('');

        // Save ESI tags and replace with some random string
        $esiTags = [];
        $html = preg_replace_callback('/(<esi:include[^>]*>)/', function($matches) use (&$esiTags) {
            foreach ($matches as $match) {
                $placeholder = '---esi-placeholder---' . uniqid('', true);
                $esiTags[$placeholder] = $match;
            }
            return $placeholder;
        }, $html);
        $htmlParser = new HTML5();
        try {
            $dom = $htmlParser->loadHTML($html);
        } catch (\Exception $exception) {
            return;
        }

        // remove linked stylesheets
        collect($dom->getElementsByTagName('link'))
            ->each(function ($node) {
                $href = $node->attributes->getNamedItem('href');

                if (!$href || Str::startsWith($href->textContent, 'http')) {
                    return;
                }
                $relValue = $node->attributes->getNamedItem('rel');
                if (!$relValue)
                    return;

                $asValue = $node->attributes->getNamedItem('as');
                if ($asValue) {
                    $asValue = $asValue->textContent;
                }

                $relValue = $relValue->textContent;
                if (!Str::contains($relValue, 'stylesheet') && (!$asValue || !Str::contains($asValue, 'style')))
                    return;

                $node->parentNode->removeChild($node);
            });

        // remove style tags
        collect($dom->getElementsByTagName('style'))
            ->each(function($node) {
                $node->parentNode->removeChild($node);
            });

        // append filtered css
        $filteredCSS = $dom->createDocumentFragment();
        $filteredCSS->appendXML("<style>$style</style>");
        $dom->getElementsByTagName('head')->item(0)->appendChild($filteredCSS);

        $htmlWithoutStyle = $htmlParser->saveHTML($dom);

        $htmlWithoutStyle = str_replace(array_keys($esiTags), array_values($esiTags), $htmlWithoutStyle);

        $response->setContent($htmlWithoutStyle);
    }

    private function htmlFromCrawler(Crawler $crawler): string
    {
        return collect($crawler)
            ->map(function($domElement) {
                return $domElement->ownerDocument->saveHTML($domElement);
            })
            ->join('');
    }
}
