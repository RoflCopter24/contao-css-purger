<?php
declare(strict_types=1);

namespace RoflCopter24\ContaoCssPurgerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    /**
     * @inheritDoc
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('contao_css_purger');

        $treeBuilder->getRootNode()
            ->children()
                ->booleanNode('enabled')
                    ->info('Controls whether the CSS purger will execute on each HTML master request')
                    ->defaultFalse()
                ->end()
                ->arrayNode('allowed_selectors')
                    ->info('CSS selectors that are exempt from purging and always included even if they are not found.')
                    ->scalarPrototype()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
