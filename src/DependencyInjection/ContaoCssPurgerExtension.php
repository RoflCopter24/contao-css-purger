<?php
declare(strict_types=1);

namespace RoflCopter24\ContaoCssPurgerBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\ConfigurableExtension;

class ContaoCssPurgerExtension extends ConfigurableExtension
{
    /**
     * @inheritDoc
     */
    protected function loadInternal(array $mergedConfig, ContainerBuilder $container)
    {
        $this->populateConfigToParameterBag($mergedConfig, $container);

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );

        $loader->load('services.yaml');
    }

    private function populateConfigToParameterBag(array $config, ContainerBuilder $containerBuilder)
    {
        foreach ($config as $name => $value) {
            $containerBuilder->setParameter("contao_css_purger.$name", $value);
        }
    }

}
