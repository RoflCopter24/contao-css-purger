<?php

use RoflCopter24\ContaoCssPurgerBundle\Service\StylePurgeService;

$GLOBALS['TL_PURGE']['custom']['purgeCssOutputCache'] = array(
    'callback' => array(StylePurgeService::class, 'clearOutputCache'),
);
$GLOBALS['TL_PURGE']['custom']['purgeStylesheetCache'] = array(
    'callback' => array(StylePurgeService::class, 'clearStylesheetCache'),
);
