<?php
declare(strict_types=1);

namespace RoflCopter24\ContaoCssPurgerBundle\Service;

use Illuminate\Support\Str;
use RoflCopter24\ContaoCssPurgerBundle\Util\CssBlockHashTable;
use RoflCopter24\ContaoCssPurgerBundle\Util\CssPurgeHtmlCrawler;
use Sabberworm\CSS\OutputFormat;
use Sabberworm\CSS\Parser;
use Sabberworm\CSS\RuleSet\DeclarationBlock;
use Sabberworm\CSS\Settings;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

class StylePurgeService
{
    private array $allowedSelectors = [
        'html',
        'body',
        'header',
        'footer',
        'main',
        'a',
        '.tiny-slider',
        '.gp-carousel',
        '.tns-',
        '.memorials-overview',
        '.memorial-search-bar',
        '.choices',
        '.aos',
        '.open',
        '.show',
        '.collapse',
        '.collapsing',
        '.collapsed',
        '.opening',
    ];
    private TagAwareCacheInterface $cache;

    public function __construct(TagAwareCacheInterface $cache, ParameterBagInterface $parameterBag)
    {
        $this->cache = $cache;
        if ($parameterBag->has('contao_css_purger.allowed_selectors')) {
            $this->allowedSelectors = array_merge(
                $this->allowedSelectors,
                $parameterBag->get('contao_css_purger.allowed_selectors')
            );
        }
    }

    public function removeUnusedCSSFromHtml(string $htmlContent, string $cssContent): string
    {
        $htmlHash = md5($htmlContent);
        $cssHash = md5($cssContent);

        // return cached version if it exists and compute it if it does not.
        return $this->cache->get("contao_css_purger.output.$htmlHash.$cssHash",
            function (ItemInterface $cacheItem) use ($htmlContent, $cssContent){
                $css = $this->filterCss($cssContent, new CssPurgeHtmlCrawler($htmlContent));
                $cacheItem->set($css);
                $cacheItem->tag('contao_css_purger.output');
                $cacheItem->expiresAfter(\DateInterval::createFromDateString('2 days'));
                return $css;
            }
        );

    }

    public function clearOutputCache(): void
    {
        $this->cache->invalidateTags(['contao_css_purger.output']);
        $this->cache->commit();
    }

    public function clearStylesheetCache(): void
    {
        $this->cache->invalidateTags(['contao_css_purger.stylesheet']);
        $this->cache->commit();
    }

    private function filterCss(string $cssContent, CssPurgeHtmlCrawler $crawler): string
    {
        $cssContentHash = md5($cssContent);

        // parsing CSS takes ages, so use the cache when possible
        $cssDoc = $this->cache->get("contao_css_purger.stylesheet.$cssContentHash",
            function (ItemInterface $cacheItem) use ($cssContent, $crawler){
                $cssParserSettings = Settings::create()->withMultibyteSupport(true);
                $cssParser = new Parser($cssContent, $cssParserSettings);
                $cssDoc = $cssParser->parse();

                // cache the parsed cssDoc
                $cacheItem->set($cssDoc);
                $cacheItem->tag('contao_css_purger.stylesheet');
                return $cssDoc;
            });

        $hashTable = new CssBlockHashTable($cssDoc);
        $hashTable = $this->analyzeUsage($crawler, $hashTable);

        foreach ($hashTable->toArray() as $block) {
            if (!$block[CssBlockHashTable::IS_USED]) {
                $cssDoc->remove($block[CssBlockHashTable::BLOCK_VALUE]);
            }
        }
        return $cssDoc->render(OutputFormat::createCompact());
    }

    private function analyzeUsage(CssPurgeHtmlCrawler $crawler, CssBlockHashTable $hashTable): CssBlockHashTable
    {
        foreach ($hashTable->toArray() as $block) {
            $decBlock = $block[CssBlockHashTable::BLOCK_VALUE];

            $blockUsed = $this->isInUse($decBlock, $crawler);

            if ($blockUsed) {
                $hash = $hashTable->hashBlock($decBlock);
                $hashTable->setUsedFlag($hash, true);
            }
        }
        return $hashTable;
    }

    private function isInUse(DeclarationBlock $block, CssPurgeHtmlCrawler $crawler): bool
    {
        $allowedSelectors = collect($this->allowedSelectors);
        foreach ($block->getSelectors() as $selector) {

            $processedSelector = $this->preprocessSelector($selector->getSelector());

            try {
                $onAllowedList = $allowedSelectors->first(function(string $allowedSelector) use ($processedSelector) {
                    return Str::contains($processedSelector, $allowedSelector);
                });

                if ($onAllowedList) {
                    return true;
                }
                if (count($crawler->findFirstInstance($processedSelector))) {
                    return true;
                };
            } catch (\Exception $exception) {
                // ignore
            }
        }
        return false;
    }

    /**
     * Strip any pseudo classes out of the selector
     *
     * @param string $selector
     *
     * @return string
     *      A processed selector string
     */
    public function preprocessSelector(string $selector): string
    {

        /*if (strstr($selector, ':')) {
            $processedSelector = preg_replace('/\([^()]+\)|::?[^ ,:.(]+/i', '', $selector);
        }
        else {*/
            $processedSelector = $selector;
        //}

        return $processedSelector;
    }
}
