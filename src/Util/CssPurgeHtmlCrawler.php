<?php
declare(strict_types=1);

namespace RoflCopter24\ContaoCssPurgerBundle\Util;

use Symfony\Component\CssSelector\CssSelectorConverter;
use Symfony\Component\CssSelector\Exception\ParseException;
use Symfony\Component\DomCrawler\Crawler;

class CssPurgeHtmlCrawler extends Crawler
{
    /**
     * Modify the xPath query provided by CssSelector to
     * find the first instance of a given element, this reduces
     * search times compared to filter which returns a whole collection
     *
     * @param string $selector
     *      A css selector
     *
     * @return Crawler
     *      The first instance of selected element
     * @throws ParseException
     */
    public function findFirstInstance(string $selector): Crawler
    {
        $sc = new CssSelectorConverter();
        try {
            $xPath = $sc->toXPath($selector);
            $xPath = '(' . $xPath . ')[1]';

            return $this->filterXPath($xPath);
        }
        catch (ParseException $e) {

            throw new ParseException("{$e->getMessage()} Invalid Selector: $selector");
        }
    }
}
