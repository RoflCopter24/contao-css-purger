<?php
declare(strict_types=1);

namespace RoflCopter24\ContaoCssPurgerBundle\Util;

use Sabberworm\CSS\CSSList\Document;
use Sabberworm\CSS\RuleSet\DeclarationBlock;

class CssBlockHashTable
{
    private array $hashTable = [];


    const IS_USED = 'isUsed';


    const BLOCK_VALUE = 'block';


    /**
     * Build a hash table of declaration blocks from
     * a given Document object
     *
     * @param Document $css
     */
    public function __construct(Document $css)
    {
        foreach ($css->getAllDeclarationBlocks() as $block) {
            $hash = $this->hashBlock($block);

            $this->hashTable[$hash] = [ self::BLOCK_VALUE => $block, self::IS_USED => false ];
        }
    }



    /**
     * Serialize the contents of the Declaration block and hash the result
     *
     * @param DeclarationBlock $block
     *
     * @return string
     *      An md5 hash representing the DeclarationBlock object
     */
    public function hashBlock(DeclarationBlock $block): string
    {
        return md5(serialize($block));
    }



    /**
     * Set the used flag for the given block at a given hash index
     *
     * @param string $hash
     *      A hashed representation of a DeclarationBlock
     *
     * @param bool $value
     *      The boolean value to set the flag as
     */
    public function setUsedFlag(string $hash, bool $value): void
    {
        $this->hashTable[$hash][self::IS_USED] = $value;
    }



    /**
     * Get the flag value for the block at a given hash index
     *
     * @param string $hash
     *      A hashed representation of a DeclarationBlock
     *
     * @return bool
     *      The value of the flag
     */
    public function getUsedFlag(string $hash): bool
    {
        return $this->hashTable[$hash][self::IS_USED];
    }



    /**
     * Get the block at a given hash index
     *
     * @param string hash
     *      A hashed representation of a DeclarationBlock
     *
     * @return DeclarationBlock
     *      The declaration block at index $hash
     */
    public function getBlock(string $hash): DeclarationBlock
    {
        return $this->hashTable[$hash][self::BLOCK_VALUE];
    }



    /**
     * Set a hashTable value
     *
     * @param DeclarationBlock $block
     *
     * @param bool $value a boolean flag to set IS_USED with
     */
    public function setBlock(DeclarationBlock $block, bool $value = false)
    {

        $hash = $this->hashBlock($block);

        $this->hashTable[$hash] = [ self::BLOCK_VALUE => $block, self::IS_USED => $value ];
    }



    /**
     * @return array a copy of the hashTable
     */
    public function toArray(): array
    {
        return $this->hashTable;
    }



    /**
     * Determine if a block is in the hash table
     *
     * @param DeclarationBlock $block
     *
     * @return bool
     *      True if block is found
     */
    public function hasBlock(DeclarationBlock $block): bool
    {
        $hash = $this->hashBlock($block);

        return isset($this->hashTable[$hash]);
    }
}
