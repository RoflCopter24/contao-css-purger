<?php
declare(strict_types=1);

namespace RoflCopter24\ContaoCssPurgerBundle\ContaoManager;

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use RoflCopter24\ContaoCssPurgerBundle\ContaoCssPurgerBundle;

class Plugin implements BundlePluginInterface
{
    /**
     * @inheritDoc
     */
    public function getBundles(ParserInterface $parser): array
    {
        return [
            BundleConfig::create(ContaoCssPurgerBundle::class)
                ->setLoadAfter([
                    ContaoCoreBundle::class
                ])
        ];
    }
}
